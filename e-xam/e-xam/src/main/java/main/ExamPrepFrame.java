package main;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import main.WebcamFrame;
import main.App;

public class ExamPrepFrame extends JFrame {

	public static JFrame frame2 = null;

	public ExamPrepFrame() {

	}
	
	static Thread t1 = null;
	static Thread t2 = null;
	static Thread t3 = null;
	public void baslat(){

		frame2 = new JFrame();
		frame2.setVisible(true);
		frame2.setSize(500, 300);
		JTextArea kılavuz = new JTextArea("Hosgeldiniz \n\n"
				+ "1) Sınav sırasında kamera ile belli aralıklarla fotoğrafınız çekilecektir.\n"
				+ "2) Sınav sırasında berlirli aralıklarla ekran görüntünüz alınacaktır. \n"
				+ "3) Sınav sırasında belirli aralıklarla ses kaydınız alınacaktır. \n"
				+ "4) Sınav sırasında açtığınız diğer sekmelerin bilgisi öğretmeniniz ile paylaşılacaktır.\n"
				+ "5) Sınav sırasında açtığınız ya da açık olan uygulamalar öğretmeniniz ile paylaşılacaktır. \n"
				+ "Lütfen sınava girmeden kapatınız.\n"
				+ "6) Lütfen kameranın görüşünden çıkmamaya özen gösteriniz eğer uzun süre görüşten \n"
				+ "çıkarsanız sınavınız sona ericektir.\n"
				+ "Kuralları kabul ediyorsanız lütfen \"Kabul Ediyorum\" u işaretleyin. \nTeşekkürler :)"
				

		);

		kılavuz.setOpaque(true);
		
		//kılavuz.setMaximumSize(new Dimension(300, 100));
		final JCheckBox check=new JCheckBox("Kabul Ediyorum");
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		frame2.add(panel);
		panel.add(kılavuz);
		panel.setMaximumSize(new Dimension(500, 220));
		panel.add(check);
		JButton buton = new JButton("Sinava basla");
		panel.add(buton);
		
		
		buton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(!(check.isSelected())){
					JOptionPane.showMessageDialog(null, "Kurallari kabul etmelisiniz", "Hata", JOptionPane.ERROR_MESSAGE);
				}
				else{
					//RunningProcesses rp = new RunningProcesses();
					//CaptureAudio ca=new CaptureAudio();
					//FaceDetection fd=new FaceDetection();

					//Thread t1 = new Thread(fd);
					//Thread t2 = new Thread(rp);
					//Thread t3 = new Thread(ca);
					
					//t1.start();
					//t2.start();
					//t3.start();

					rpStart();
					fdStart();
					caStart();
					
					
					// App ap = new App();
					frame2.setVisible(false);
				}
				
			}
		});

		// frame.setTitle("Exam Prep");
		// frame.pack();

	}
	
	public void rpStart(){
		RunningProcesses rp = new RunningProcesses();
		t1 = new Thread(rp);
		t1.start();
	}
	public void rpStop(){
		t1.interrupt();
	}
	
	public void caStart(){
		CaptureAudio ca=new CaptureAudio();

		t2 = new Thread(ca);
		t2.start();
	}
	public void caStop(){
		t2.interrupt();
	}
	
	public void fdStart(){
		FaceDetection fd=new FaceDetection();

		t3 = new Thread(fd);
		t3.start();
	}
	public void fdStop(){
		t3.interrupt();
	}
	
	

	public static void main(String[] args) {

	}
}