package main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class FirstFrame extends JPanel {
	public JLabel name = null;
	public JLabel baslik = null;
	public JLabel password = null;
	public JTextField textname = null;
	public JTextField textpassword = null;
	public JButton ok = null;
	static JFrame frame = null;

	public FirstFrame() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		baslik = new JLabel();
		baslik.setText("Please, Login!");
		// baslik.setIconTextGap(5);
		baslik.setMaximumSize(new Dimension(250, 30));
		baslik.setIconTextGap(3);
		add(baslik);
		JPanel namePanel = new JPanel();
		namePanel.setLayout(new BoxLayout(namePanel, BoxLayout.X_AXIS));
		name = new JLabel();
		name.setText("   User Name");
		name.setMaximumSize(new Dimension(100, 50));
		namePanel.add(name);
		textname = new JTextField("Name Surname");
		textname.setMaximumSize(new Dimension(250, 30));
		namePanel.add(textname);

		JPanel passwordPanel = new JPanel();
		passwordPanel.setLayout(new BoxLayout(passwordPanel, BoxLayout.X_AXIS));
		password = new JLabel();
		password.setText("   Password");
		password.setMaximumSize(new Dimension(105, 90));
		passwordPanel.add(password);

		textpassword = new JPasswordField("123456");
		textpassword.setMaximumSize(new Dimension(250, 30));
		passwordPanel.add(textpassword);

		add(namePanel);
		add(passwordPanel);
		ok = new JButton();
		ok.setText("LOGIN");
		ok.setMaximumSize(new Dimension(100, 30));
		add(ok);
		ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("logine tiklandi");
				ExamPrepFrame epf = new ExamPrepFrame();
				epf.baslat();
				frame.setVisible(false);
			}
		});
	}

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				FirstFrame gui = new FirstFrame();
				frame = new JFrame();
				frame.setTitle("SECURE");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.getContentPane().add(gui);
				frame.pack();
				frame.setVisible(true);
				frame.setSize(300, 225);
			}
		});
	}
}
